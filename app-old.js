const http = require('http');

http.createServer((request, response) => {
    response.writeHead(200, { 'Content-Type': 'application/json' }); //el código 200 significa que el proceso fue exitoso

    let salida = {
        nombre: 'Cristal',
        edad: 21,
        url: request.url
    }

    response.write(JSON.stringify(salida));
    response.end();
}).listen(8080); //Escucha el puerto 8080

console.log('Conectado mediante el puerto 8080');