http
express
hbs
    helpers y partials
heroku
handlebar

## Código con express y hbs

En esta sección, utilicé express para usar el servicio http en una página genérica. También, utilicé partials y helpers para poder reutilizar fragmentos de una página en las demás y reutilizar variables en las páginas que se necesiten. Para esto, se utilizó hbs, que es un paquete que renderiza las páginas html.
Para públicar las páginas junto con el servicio de peticiones, se utilizó heroku, el cual es muy similar a GitHub o GitLab en cuanto a manejo.

Para utilizar esta aplicación es necesario instalar npm con el siguiente comando:

```
npm install
```