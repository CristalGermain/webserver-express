const express = require('express');
const app = express();
const hbs = require('hbs');
require('./hbs/helpers');


const port = process.env.PORT || 3000;


app.use(express.static(__dirname + '/public')); //responde con cualquier URL, es público

//Express HBS engine
hbs.registerPartials(__dirname + '/views/parciales');
app.set('view engine', 'hbs');



app.get('/', (request, response) => {

    response.render('home', {
        nombre: 'cRistaL gerMaIn'
    });
    //response.send(salida);

});

app.get('/about', (request, response) => {

    response.render('about');
});

app.listen(port, () => {
    console.log(`Escuchando peticiones por el puerto ${port}`);
});