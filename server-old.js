const express = require('express');
const app = express();

app.get('/', (request, response) => {

    response.send('Hola mundo');

});

app.get('/data', (request, response) => {

    let salida = {
        nombre: 'Cristal',
        edad: 21,
        url: request.url
    };
    response.send(salida);

});

app.listen(3000, () => {
    console.log('Escuchando peticiones por el puerto 3000');
});